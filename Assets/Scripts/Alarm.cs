﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Alarm : MonoBehaviour {


	GameObject enemy;
	GameObject loseText;
	public static bool AlarmTriggered;
	public Image damageImage;
	public float flashSpeed = 5f;
	public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
	float timer;


	void Start () {
		
	}

	void Awake(){
		enemy = GameObject.FindGameObjectWithTag ("Enemy");

	}

	void Update () {

		timer += Time.deltaTime;

		if (AlarmTriggered == true) {
			//SpawnArmy ();

			print ("Alarm goes off and you lose");

			damageImage.color = flashColour;
			//damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);

			if (timer >= 1) {
				//damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
				damageImage.color = flashColour;
				timer = 0;
				print (timer);
			} else {
				if (timer < 1) {
					damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
					print ("should be lerpin: " + timer);
				}
			}
		}
	}
			
	void OnTriggerEnter (Collider other) //// on raycast hit
	{
		if(other.gameObject == enemy)
		{
			AlarmTriggered = true;
			//winShow.GetComponent.<Text>().enabled = true; 
		}
	}

}
