﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemiesRemaining : MonoBehaviour
{
	public static int remaining;


	Text text;


	void Awake ()
	{
		text = GetComponent <Text> ();
		remaining = 5;
	}


	void Update ()
	{
		text.text = "Enemies Remaining: " + remaining;
		if (remaining == 0) {
			GetComponent<UnityEngine.UI.Text>().enabled = true; 
		}
	}
}
