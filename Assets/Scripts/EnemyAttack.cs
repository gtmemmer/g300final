﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
	public float timeBetweenAttacks = 0.5f;
	public int attackDamage = 10;


	//Animator anim;
	GameObject player;
	PlayerHealth playerHealth;
	EnemyHealth enemyHealth;
	bool playerInRange;
	float timer;
	bool isSinking;
	public int damagePerShot = 20;
	public float timeBetweenBullets = 0.15f;
	public float range = 100f;


	Ray shootRay = new Ray();
	RaycastHit shootHit;
	int shootableMask;
	ParticleSystem gunParticles;
	LineRenderer gunLine;
	//AudioSource gunAudio;
	Light gunLight;
	float effectsDisplayTime = 0.2f;


	void Awake ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
		enemyHealth = GetComponent<EnemyHealth>();
		//anim = GetComponent <Animator> ();
		gunLight = GetComponentInChildren<Light> (); 
		shootableMask = LayerMask.GetMask ("Shootable");
		gunParticles = GetComponentInChildren<ParticleSystem> ();
		gunLine = GetComponentInChildren <LineRenderer> ();
		//GameObject theEnemy = GameObject.Find("Enemy");
		//EnemyMovement enemyMove = theEnemy.GetComponent<EnemyMovement> ();
	}


	void OnTriggerEnter (Collider other) //// on raycast hit
	{
		if(other.gameObject == player)
		{
			playerInRange = true;
		}
	}


	void OnTriggerExit (Collider other)
	{
		if(other.gameObject == player)
		{
			playerInRange = false;
		}
	}

	void Shoot ()
	{
		timer = 0f;

		//gunAudio.Play ();

		gunLight.enabled = true;

		gunParticles.Stop ();
		gunParticles.Play ();

		gunLine.enabled = true;
		gunLine.SetPosition (0, transform.position);

		shootRay.origin = transform.position;
		shootRay.direction = transform.forward;

		if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
		{
			PlayerHealth playerHealth = shootHit.collider.GetComponent <PlayerHealth> ();
			if(playerHealth != null)
			{
				playerHealth.TakeDamage (damagePerShot);                        
			}
			gunLine.SetPosition (1, shootHit.point);
		}
		else
		{
			gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
		}
	}

	public void StartSinking ()
	{
		//print ("startsinkingfunction");
		GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
		GetComponent <Rigidbody> ().isKinematic = true;
		isSinking = true;
		//ScoreManager.score += scoreValue;
		Destroy (gameObject, 2f);
	}

	void Update ()
	{
		timer += Time.deltaTime;

		if(timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
		{
			Attack ();
			//print (playerHealth.currentHealth);
		}

		if(playerHealth.currentHealth <= 0)
		{
			//anim.SetTrigger ("PlayerDead");
			//StartSinking ();

		}
	}


	void Attack ()
	{
		timer = 0f;

		if(playerHealth.currentHealth > 0) //&&  (GameObject.Find("Enemy").GetComponent<EnemyMovement>().decision == 1) )
		{
			//playerHealth.TakeDamage (attackDamage);
			Shoot ();
		}
	}
}
