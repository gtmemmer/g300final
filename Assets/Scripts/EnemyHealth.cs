﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

	public int startingHealth = 100;
	public int currentHealth;
	public float sinkSpeed = 2.5f;
	public int scoreValue = 10;
	//public AudioClip deathClip;

	//EnemiesRemaining remaining;
	//Animator anim;
	//AudioSource enemyAudio;
	ParticleSystem hitParticles;
	CapsuleCollider capsuleCollider;
	SphereCollider sphereCollider;
	bool isDead;
	bool isSinking;


	void Awake ()
	{
		//anim = GetComponent <Animator> ();
		//enemyAudio = GetComponent <AudioSource> ();
		hitParticles = GetComponentInChildren <ParticleSystem> ();
		capsuleCollider = GetComponent <CapsuleCollider> ();
		sphereCollider = GetComponent<SphereCollider> ();
		//GameObject remaining = GameObject.Find ();
		//EnemiesRemaining remainScript = remaining.GetComponent<EnemiesRemaining> ();
		//GetComponent <EnemiesRemaining> ();
		//remaining = GetComponent <EnemiesRemaining> ();
		currentHealth = startingHealth;
		//remainScript.remaining -= 1;

	}


	void Update ()
	{
		if(isSinking)
		{
			transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);

		}
	}


	public void TakeDamage (int amount, Vector3 hitPoint)
	{
		//if(isDead)
			//return;

		//enemyAudio.Play ();

		currentHealth -= amount;

		//hitParticles.transform.position = hitPoint;
		//hitParticles.Play();

		if(currentHealth <= 0)
		{
			Death ();

		}
	}


	void Death ()
	{
		isDead = true;
		print ("isdead true");
		capsuleCollider.enabled = false;
		sphereCollider.enabled = false;
		StartSinking ();
		EnemiesRemaining.remaining -= 1;
		//-1 from remaining enemies

		//enemyAudio.clip = deathClip;
		//enemyAudio.Play ();
	}


	public void StartSinking ()
	{
		print ("startsinkingfunction");
		GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
		GetComponent <Rigidbody> ().isKinematic = true;
		isSinking = true;
		//ScoreManager.score += scoreValue;
		Destroy (gameObject, 2f);
	}
}
