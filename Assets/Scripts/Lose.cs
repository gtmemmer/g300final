using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lose : MonoBehaviour {

	PlayerHealth playerHealth;
	GameObject player;
	Alarm alarm;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
		alarm = GetComponent <Alarm> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (playerHealth.isDead == true){
			GetComponent<UnityEngine.UI.Text>().enabled = true; 
		}
		if (Alarm.AlarmTriggered == true) {
			GetComponent<UnityEngine.UI.Text>().enabled = true; 
		}
	}	
}
