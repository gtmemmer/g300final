﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		GetComponent<EnemiesRemaining> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (EnemiesRemaining.remaining == 0) {
			GetComponent<UnityEngine.UI.Text> ().enabled = true; 
		}
	}
}
