﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
	public int startingHealth = 100;
	public int currentHealth;
	public Slider healthSlider;
	public Image damageImage;
	//public AudioClip deathClip;
	public float flashSpeed = 5f;
	public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
	public float sinkSpeed = 2.5f;


	//Animator anim;
	//AudioSource playerAudio;
	PlayerMovement playerMovement;
	PlayerShooting playerShooting;
	public bool isDead;
	bool damaged;
	bool isSinking;


	void Awake ()
	{
		//anim = GetComponent <Animator> ();
		//playerAudio = GetComponent <AudioSource> ();
		playerMovement = GetComponent <PlayerMovement> ();
		playerShooting = GetComponentInChildren <PlayerShooting> ();
		currentHealth = startingHealth;
	}


	void Update ()
	{
		if(damaged)
		{
			damageImage.color = flashColour;
		}
		else
		{
			damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
		}
		damaged = false;

		if(isSinking)
		{
			transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
		}
	}


	public void TakeDamage (int amount)
	{
		damaged = true;

		currentHealth -= amount;
		print ("Health is: " + currentHealth);
		healthSlider.value = currentHealth;

		//playerAudio.Play ();

		if(currentHealth <= 0 && !isDead)
		{
			Death ();
		}
	}

	public void StartSinking ()
	{
		print ("startsinkingfunction for player");
		//GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
		GetComponent <Rigidbody> ().isKinematic = true;

		//ScoreManager.score += scoreValue;
		Destroy (gameObject, 2f);
	}

	void Death ()
	{
		isDead = true;

		playerShooting.DisableEffects ();
		StartSinking ();
		//anim.SetTrigger ("Die");

		//display the death text

		playerMovement.enabled = false;
		playerShooting.enabled = false;
		isSinking = true;
	}


	public void RestartLevel ()
	{
		//SceneManager.LoadScene (0);
	}
}
