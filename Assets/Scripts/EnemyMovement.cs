﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

	Transform player;
	Transform alarm;
	public Transform[] path;
	public float speed;
	public float reachDist = 1.0f;
	public int currentPoint = 0;
	public float decision;
		//public int decision = Random.Range (1, 2);

	UnityEngine.AI.NavMeshAgent nav;
	private int current;

	PlayerHealth playerHealth;
	EnemyHealth enemyHealth;
	//public WinngZone inZone;
	public bool playerInWin;
	public bool alerted;

	void Start () {
		decision = Random.value;
	//	print ("Decision: " + decision);
	}

	void Awake ()
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		playerHealth = player.GetComponent <PlayerHealth> ();
		enemyHealth = GetComponent <EnemyHealth> ();
		alarm = GameObject.FindGameObjectWithTag ("Alarm").transform;
		nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
		//float decision = Random.Range(0.0f, 1);
	}

	void FollowPath (){
		if ((transform.position - path [current].position).magnitude > .05f) {
			Vector3 pos = Vector3.MoveTowards (transform.position, path [current].position, speed * Time.deltaTime);
			GetComponent<Rigidbody> ().MovePosition (pos);
			Vector3 forward = new Vector3 (path [current].position.x - transform.position.x, 0, path [current].position.z - transform.position.z); //use nav to get back to a point
			transform.rotation = Quaternion.LookRotation (forward, transform.up);
		} else {
			current = (current + 1) % path.Length;
		}
	}

	void Update ()
	{
		if (alerted == false) {
			FollowPath ();
		}
	}


	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			other.GetComponent<PlayerMovement> ().closeSwitch = this;
		//	print ("Attack " + decision);
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (other.gameObject.CompareTag ("Player")) {
			if( other.GetComponent<PlayerMovement>().closeSwitch == this)
				other.GetComponent<PlayerMovement>().closeSwitch = null;
			
		}
	}

	public void AttackPlayer()
	{
		if(enemyHealth.currentHealth > 0 && (playerHealth.currentHealth > 0 ) )
		{
			nav.SetDestination (player.position);
		}
		else
		{
			nav.enabled = false;
			alerted = false;
			FollowPath ();
		}
	}

	public void SoundAlarm ()
	{
		if (enemyHealth.currentHealth > 0 && (playerHealth.currentHealth > 0)) {
			nav.SetDestination (alarm.position);
			nav.speed = 7;
		} else {
			//need the enemy to freeze? maybe?
		}	
	}
		
	public void SwitchAction()
	{
		alerted = true; 
		//print ("Descision: " + decision);
		//int decision = Random.Range (1, 2);
		//Random number 1:2
		if (decision < .01f) { 
			AttackPlayer ();

		} else if (decision > .01f) {
				SoundAlarm ();
			//print ("Run" + decision);
			}
		}
	}
